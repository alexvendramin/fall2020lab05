package movies.importer;

import java.io.*;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		LowercaseProcessor object = new LowercaseProcessor("C:\\Users\\alexv\\Documents", "C:\\Users\\alexv\\Documents\\Assignments\\Java 3");
		object.execute();
		
		RemoveDuplicates object2 = new RemoveDuplicates("C:\\Users\\alexv\\Documents//Assignments//Java 3", "C:\\Users\\alexv\\Documents\\Assignments\\Java 3\\lab05TestFolder");
		object2.execute();
	}
}
