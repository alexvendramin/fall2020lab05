package movies.importer;

import java.util.*;

public class RemoveDuplicates extends Processor{
	
	public RemoveDuplicates(String src, String dest) {
		super(src, dest, false);
	}
	
	public ArrayList<String> process(ArrayList<String> array){
		String temp;
		for(int i = 0; i < array.size(); i++) {
			temp = array.get(i);
			array.remove(i);
			if(array.contains(temp)) {
				array.remove(temp);
				array.add(temp);
			}
			else {
				array.add(temp);
			}
		}
		return array;
	}
}
