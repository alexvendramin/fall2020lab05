package movies.importer;

import java.util.*;

public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String src, String dest){
		super(src, dest, true);
	}
	
	public ArrayList<String> process(ArrayList<String> array){
		ArrayList<String> asLower = new ArrayList<String>();
		String temp;
		for(int i = 0; i < array.size(); i++) {
			temp = array.get(i);
			temp = temp.toLowerCase();
			asLower.add(temp);
		}
		return asLower;
	}
}